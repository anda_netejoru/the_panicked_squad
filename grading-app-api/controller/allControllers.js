// const juryService = require('./../service/jury');
// const phaseService = require('./../service/phase');
// const projectService = require('./../service/project');
// const userService = require('./../service/user');
var moment = require('moment');
var CronJob = require('cron').CronJob;
const{
    juryService,
    phaseService,
    projectService,
    userService,
    teamService,
    serverTimeService
    
}=require('./../service/allServices');

//Jury
///////////////////////////////////////////////////////////////
//@@@
const getJuryMemberByUserId=async (req, res, next) => {
    const userId = req.params.userId;
    if(userId) {
        const result = await juryService.getJuryMemberByUserId(userId);
        res.status(201).send(result);
    } else {
        res.status(400).send({
            message: 'Invalid userId .'
        });
    }
}

const createJuryMember = async (req, res, next) => {
    const jury = req.body;
    if(jury) {
        const result = await juryService.create(jury);
        res.status(201).send({
            message: 'Jury member added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid Jury Member payload.'
        });
    }
}

const getAllJuryMembers = async (req, res, next) => {
    try {
        const juryMembers = await juryService.getAll();
        res.status(200).send(juryMembers);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


const getJuryMembersByProjectId = async (req, res, next) => {
    try {
        const id = req.params.id;
        if(id) {
            try {
                const juryMembers = await juryService.getJuryMembersByProjectId(id);
                res.status(200).send(juryMembers);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No userId specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


const getMaxGrade = async (req, res, next) => {
    try{
      const projectId = req.params.projectId;
      if (projectId){
          try{
              const maxGrade = await juryService.getMaxGrade(projectId);
              res.status(200).send(maxGrade);
          }
          catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      }
      else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
        
    }catch(err){
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


const getMinGrade = async (req, res, next) => {
    try{
      const projectId = req.params.projectId;
      if (projectId){
          try{
              const minGrade = await juryService.getMinGrade(projectId);
              res.status(200).send(minGrade);
          }
          catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      }
      else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
        
    }catch(err){
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getSumOfGrades = async(req,res,next)=>{
    try{
        const projectId=req.params.projectId;
        if(projectId){
            try{
                const sum = await juryService.getSumOfGrades(projectId);
                    
                res.status(200).send(sum);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      }
      else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
    }catch(err){
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getNumberOfJuryMembers= async(req,res,next)=>{
    try{
        const projectId=req.params.projectId;
        if(projectId){
            try{
                const sum = await juryService.getNumberOfJuryMembers(projectId);
                    
                res.status(200).send(sum);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      }
      else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
    }catch(err){
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

///



//


//adauga un jurat pentru un proiect
const selectTeamUserToJury = async (req, res, next) => {
     var ok = 0;
    const projectId = req.params.FKprojectId;
    try {
        
        
        
        let allPotentialMembers = await teamService.selectAllPossibleJuryMembersForProjectX(projectId);
    //     var {ok} = {ceva:"mwegi"};
    //   (async function(){
    //         ok=2;
    //     })();
    //     res.status(200).send(ok);
    
        try{
                    for (var i = 0; i < allPotentialMembers.length; i++) {
                        let member =  await juryService.getJuryMembersByUserId(allPotentialMembers[i].FKuserId);
                        if(member==null){
                            ok=1;
                        }
                    }
        }
        catch(err)
        {
            res.status(500).send({message: `Error occured: ${err.message}`})
        }    
        //console.log(allPotentialMembers.length);
        //res.status(200).send({"size":allPotentialMembers[0].FKuserId});
        
        if(ok==1){
            let potentialMember = await teamService.selectPossibleJuryMemberForProjectX(projectId);
            // res.status(200).send(potentialMember);
            let memberExistsInJury = await juryService.getJuryMembersByUserId(potentialMember.FKuserId);
           // res.status(200).send(memberExistsInJury);
            
          
            while(memberExistsInJury != null){
                potentialMember = await teamService.selectPossibleJuryMemberForProjectX(projectId);
                memberExistsInJury = await juryService.getJuryMembersByUserId(potentialMember.FKuserId);
                i++;
            }
            
            if(memberExistsInJury == null){
                const juryMember = await juryService.create({FKprojectId:projectId,FKuserId:potentialMember.FKuserId});
                res.status(200).send(juryMember);
            }
        }
        else
        {
             res.status(500).send({message: "OK 0"})
        }
        
    }catch(err){
         res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    } 
        
}


///////////////////////////////////////////////////////////////



//Phase
///////////////////////////////////////////////////////////////
const createPhase= async (req, res, next) => {
    const phase = req.body;
    //de schimbat if
    if(phase.deadline && phase.phaseNumber) {
        const result = await phaseService.create(phase);
        res.status(201).send({
            message: 'Phase added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid phase payload.'
        });
    }
}

const getAllPhases = async (req, res, next) => {
    try {
        const phases = await phaseService.getAll();
        res.status(200).send(phases);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

const getPhasesByDeadline = async (req, res, next) => {
    try {
        const deadline = req.params.deadline;
        if(deadline) {
            try {
                const phases = await phaseService.getPhasesByDeadline(deadline);
                res.status(200).send(phases);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No deadline specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getPhasesByProjectId = async (req, res, next) => {
    try {
        const id = req.params.id;
        if(id) {
            try {
                const phase = await phaseService.getPhasesByProjectId(id);
                res.status(200).send(phase);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No phaseId specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const updatePhaseLink = async (req, res, next) => {  // pt Phase X
    try {
        //idPhase=phaseNumber
        const idPhase = req.params.id;
        const idProject = req.params.FKprojectId;
        //const link = req.params.link;
        const link=req.body.link;
        if(idPhase && idProject) {
            try {
                const phase = await phaseService.updatePhaseLink(idPhase,idProject,link);
                 //de schimbat cu message
               // res.status(200).send(phase);
                res.status(200).send({message:"Phase link updated successfully!"});
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id or link specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const updatePhaseDeadline = async (req, res, next) => {  // pt Phase X
    try {
         const idPhase = req.params.id;
        const idProject = req.params.FKprojectId;
        const deadline = req.params.deadline;
        if(deadline && idPhase) {
            try {
                const phase = await phaseService.updatePhaseDeadline(idPhase,idProject,deadline);
                //de schimbat
                //res.status(200).send(phase);
                 res.status(200).send({message:"Phase deadline updated successfully!"});
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id or link specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const deletePhaseAndUpdateProject = async (req, res, next) => {  
    try {
        const idProject = req.params.FKprojectId;
        const deadline = req.params.deadline;
        if(deadline && idProject) {
            try {
                const proj = await phaseService.deletePhaseAndUpdateProject(idProject,deadline);
                //de schimbat
               // res.status(200).send(proj);
                res.status(200).send({message:"Phase deleted successfully!"});
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id or deadline specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}
///////////////////////////////////////////////////////////////


//Project
///////////////////////////////////////////////////////////////
const createProject= async (req, res, next) => {
    const project = req.body;
    //de schimbat if
    if(project.deadline && project.teamName) {
        const result = await projectService.create(project);
        res.status(201).send({
            message: 'Project added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid project payload.'
        });
    }
}

const getAllProjects = async (req, res, next) => {
    try {
        const project = await projectService.getAll();
        res.status(200).send(project);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


const getProjectByDeadline = async (req, res, next) => {
    try {
        const deadline = req.params.finalDeadline;
        const projectId=req.params.id;
        if(deadline && projectId) {
            try {
                const project = await projectService.getProjectByDeadline(deadline,projectId);
                res.status(200).send(project);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No deadline specified for project'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}
//#$%@ Cron 1 - setting final grade - will run  every day at 20:00 

new CronJob('* * * *  ', function() {
// new CronJob('0 20 * * *', function() {
     //let currentTime=moment().format("YYYY-MM-DD HH:mm:ss")
    let currentTime=moment().format("YYYY-MM-DD")
  console.log(currentTime);
  //@#$% HARDCODAT!!!!
  currentTime="2019-08-09T04:05:02.000Z";
  console.log("allController cron 1");
  
    (async (req, res, next) => {
    try {
        req=currentTime;
        const deadline = req;
        if(deadline) {
            try {
                const projects = await projectService.getProjectsByDeadline(deadline);
                let projectIdFound;
                console.log("@@@@@@@@@@@@@@@@@@@@@@ 1");
                for(var i=0;i<projects.length;i++)
                {
                     projectIdFound=projects[i].dataValues.id;
                     
                      (async(req,res,next)=>{
                                try{
                                    req=projectIdFound;
                                    const projectId=req;
                                    if(projectId){
                                        try{
                                            const finalGrade = await projectService.setFinalGrade(projectId);
                                            if(finalGrade)
                                            {
                                                 //res.status(200).send(finalGrade);
                                                 
                                                 
                                                 //console.log("Final grade updated");
                                                 res.status(200).send({message:"Project final grade has been successfully updated!"});
                                            }
                                            else
                                            {
                                                res.status(200).send({message:"Not enough jury members!"});
                                            }
                                           
                                        } catch(err) {
                                            res.status(500).send({
                                                message: `Err occured: ${err.message}`
                                            })
                                        }
                                  }
                                  else {
                                        res.status(400).send({
                                            message: 'No id specified'
                                        })
                                    }
                                }catch(err){
                                    // res.status(500).send({
                                    //     message: `Error occured: ${err.message}`
                                    // })
                                }
                            })();
                     
                }
                console.log("@@@@@@@@@@@@@@@@@@@@@@ 1");
                
              res.status(200).send(projects);
              console.log(projects);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No deadline specified for project'
            })
        }
    } catch(err) {
        // res.status(500).send({
        //     message: `Error occured: ${err.message}`
        // })
    }
    console.log('zi');
})();
  
}, null, true, 'Europe/Bucharest');



///@#$% Cron 2 - Creating jury members -  - will run  every day at 22:00 
new CronJob('* * * * * ', function() {
//new CronJob('0 22 * * *', function() {
     //let currentTime=moment().format("YYYY-MM-DD HH:mm:ss")
    let currentTime=moment().format("YYYY-MM-DD")
  console.log(currentTime);
  //@#$% HARDCODAT!!!!
  currentTime="2019-08-19";
  console.log("allController cron 2 #####################");
  
  //get phases by deadline 
(async (req, res, next) => {
    try {
         req=currentTime;
        const deadline = req;
        if(deadline) {
            try {
                const phases = await phaseService.getPhasesByDeadline(deadline);
                
                console.log('###################################################');
                let projectId;
                for(let i=0;i<phases.length;i++)
                {
                    //console.log(phases[i].FKprojectId);
                    projectId=phases[i].dataValues.FKprojectId;
                    console.log(projectId);
                    //`````````````
                      console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                      (async (req, res, next) => {
                         var ok = 0;
                         
                        //const projectId = req.params.FKprojectId;
                        try {

                            let allPotentialMembers = await teamService.selectAllPossibleJuryMembersForProjectX(projectId);
                        //     var {ok} = {ceva:"mwegi"};
                        //   (async function(){
                        //         ok=2;
                        //     })();
                        //     res.status(200).send(ok);
                            try{
                                        for (var i = 0; i < allPotentialMembers.length; i++) {
                                            let member =  await juryService.getJuryMembersByUserId(allPotentialMembers[i].FKuserId);
                                            if(member==null){
                                                ok=1;
                                            }
                                        }
                            }
                            catch(err)
                            {
                                res.status(500).send({message: `Error occured: ${err.message}`})
                            }    
                            //console.log(allPotentialMembers.length);
                            //res.status(200).send({"size":allPotentialMembers[0].FKuserId});
                            
                            if(ok==1){
                                let potentialMember = await teamService.selectPossibleJuryMemberForProjectX(projectId);
                                // res.status(200).send(potentialMember);
                                let memberExistsInJury = await juryService.getJuryMembersByUserId(potentialMember.FKuserId);
                              // res.status(200).send(memberExistsInJury);
                                
                              
                                while(memberExistsInJury != null){
                                    potentialMember = await teamService.selectPossibleJuryMemberForProjectX(projectId);
                                    memberExistsInJury = await juryService.getJuryMembersByUserId(potentialMember.FKuserId);
                                    i++;
                                }
                                
                                if(memberExistsInJury == null){
                                    const juryMember = await juryService.create({FKprojectId:projectId,FKuserId:potentialMember.FKuserId});
                                    res.status(200).send(juryMember);
                                }
                            }
                            else
                            {
                                 res.status(500).send({message: "OK 0"})
                            }
                            
                        }catch(err){
                            //  res.status(500).send({
                            //     message: `Error occured: ${err.message}`
                            // })
                        } 
                            
                    })();                    
                    
                }
                console.log('###################################################');
                res.status(200).send(phases);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No deadline specified'
            })
        }
    } catch(err) {
        // res.status(500).send({
        //     message: `Error occured: ${err.message}`
        // })
    }
})();
  

}, null, true, 'Europe/Bucharest');






const getProjectsByDeadline = async (req, res, next) => {
    try {
        const deadline = req.params.finalDeadline;
        if(deadline) {
            try {
                const projects = await projectService.getProjectsByDeadline(deadline);
                res.status(200).send(projects);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No deadline specified for project'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getProjectById = async (req, res, next) => {
    try {
        const id = req.params.id;
        if(id) {
            try {
                const project = await projectService.getProjectById(id);
                res.status(200).send(project);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No projectId specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

// const createProjectWithPhases= async (req, res, next) => {
//  const project = req.body;

//     if(project.id) {
//         const result = await projectService.create(project);
        
//         if(result.noPhases)
//         {
//             for(var i=0;i<result.noPhases;i++)
//             {
//                 let phase=await phaseService.create({FKprojectId:result.id,deadline:result.finalDeadline});
//                 result.addPhases(phase);
//             }
            
//         }
//         res.status(201).send({
//             message: 'Project and phases added successfully.'
//         });
//     } else {
//         res.status(400).send({
//             message: 'Invalid project & phase payload.'
//         });
//     }
// }

//Forta Anda v2.0
const createProjectWithPhases= async (req, res, next) => {
     try {
 const project = req.body;
 const teamName = req.params.teamName;

    
        const result = await projectService.create(project);
        
        if(result.noPhases)
        {
            for(var i=0;i<result.noPhases;i++)
            {
                let phase=await phaseService.create({FKprojectId:result.id,deadline:result.finalDeadline, phaseNumber:i+1});
                result.addPhases(phase);
            }
            
            
            //result.id -> de pus in team pe teamMembers
            let teamMembers = await teamService.getTeamMembersByTeamName(teamName);
            teamMembers = await teamService.updateTeamSetProjectId(teamName, result.id);
            
             res.status(201).send({
            message: 'Project and phases added successfully.'});
        
        //  res.status(201).send(teamMembers);
        
            
        }else{
            res.status(400).send({
                message: 'No details specified'
            })
        }
       
     } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const updateProjectFinalGrade = async (req, res, next) => { 
    try {
        const grade = req.params.finalGrade;
        const id = req.params.id;
        if(grade && id) {
            try {
                const project = await projectService.updateProjectFinalGrade(id,grade);
                //res.status(200).send(project);
                res.status(200).send({message:"Project final grade has been successfully updated!"});
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id or grade specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


const setFinalGrade = async(req,res,next)=>{
    try{
        const projectId=req.params.projectId;
        if(projectId){
            try{
                const finalGrade = await projectService.setFinalGrade(projectId);
                if(finalGrade)
                {
                     //res.status(200).send(finalGrade);
                     res.status(200).send({message:"Project final grade has been successfully updated!"});
                }
                else
                {
                    res.status(200).send({message:"Not enough jury members!"});
                }
               
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      }
      else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
    }catch(err){
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


///////////////////////////////////////////////////////////////


//User
///////////////////////////////////////////////////////////////
const createUser = async (req, res, next) => {
    const user = req.body;
    if(user.password && user.role) {
        const result = await userService.create(user);
        res.status(201).send({
            message: 'User added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid user payload.'
        });
    }
}

const getAllUsers = async (req, res, next) => {
    try {
        const users = await userService.getAll();
        res.status(200).send(users);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

const getUserByRole = async (req, res, next) => {
    try {
        const role = req.params.role;
        if(role) {
            try {
                const users = await userService.getUserByRole(role);
                res.status(200).send(users);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No role specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getUserById = async (req, res, next) => {
    try {
        const id = req.params.id;
        if(id) {
            try {
                const users = await userService.getUserById(id);
                res.status(200).send(users);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getUserByEmailAndPassword = async (req, res, next) => { //pt Login
    try {
        const email = req.params.email;
        const password = req.params.password;
        if(email && password) {
            try {
                let user = await userService.getUserByEmailAndPassword(email, password);
                if(user)
                {
                res.status(200).send(user);
                }
                else
                {
                res.status(404).send({message:"No such user! - verify password or email"});
                }
            
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No credentials specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}



const getUserByEmail= async (req, res, next) => { //pt Login
    try {
        const email = req.params.email;
        
        if(email) {
            try {
                let user = await userService.getUserByEmail(email);
                if(user)
                {
                res.status(200).send(user);
                }
                else
                {
                res.status(404).send({message:"No such user! - verify the email"});
                }
            
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No credentials specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


///////////////////////////////////////////////////////////////

//Team
///////////////////////////////////////////////////////////////
const getTeamByProjectId=async (req, res, next) => {
    try {
        const projectId=req.params.FKprojectId;
        const team = await teamService.getTeamByProjectId(projectId);
        res.status(200).send(team);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


const createTeam= async (req, res, next) => {
    const team = req.body;
    if(team.teamName) {
        const result = await teamService.create(team);
        res.status(201).send({
            message: 'Team added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid team payload.'
        });
    }
}
const getAllTeams=async (req, res, next) => {
    try {
        const teams = await teamService.getAll();
        res.status(200).send(teams);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


const getTeamMembersByTeamName = async (req, res, next) => {
    try {
        const teamName = req.params.teamName;
        if(teamName) {
            try {
                const teamMembers = await teamService.getTeamMembersByTeamName(teamName);
                res.status(200).send(teamMembers);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No team name specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const updateJuryGrade = async (req, res, next) => {  
    try {
        //juryId s-a transformat in FKuserId
        const projectId = req.params.FKprojectId;
        const juryId = req.params.id;
        const grade = req.params.grade;
        if(projectId && juryId && grade) {
            try {
                const jury = await juryService.updateGrade(juryId, projectId,grade);
                //de schimbat cu message succesuff..
                
               // res.status(200).send(jury);
                res.status(200).send({message:`The grade of the jury with id ${juryId} of the project with id ${projectId} has been updated with grade ${grade}`});
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No ids or grade specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const getFinalTeamGrades = async (req, res, next) => {
        try {
            const results = await projectService.getFinalTeamGrades();
            res.status(200).send(results);
        } catch(err) {
            res.status(500).send({
                message: `Err occured: ${err.message}`
            })
        }
}

const updateTeamSetProjectId = async (req, res, next) => {
   
    try {
         const teamName = req.params.teamName;
         const projectId = req.params.FKprojectId;
            if(teamName && projectId) {
            try {
                const team = await teamService.updateTeamSetProjectId(teamName, projectId);
                res.status(200).send(team);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No projectId specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const selectPossibleJuryMembersForProjectX = async (req, res, next) => {
    try {
        const projectId = req.params.FKprojectId;
        if(projectId) {
            try {
                const possibleJuries = await teamService.selectPossibleJuryMemberForProjectX(projectId);
                res.status(200).send(possibleJuries);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No project specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}


//Server
//////////////////////////////
const getServerTime= async(req,res,next)=>{
            try{
                const serverTime = await serverTimeService.create();
                     res.status(200).send(serverTime);
                
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      
    
}
const getServerTimeLogs= async(req,res,next)=>{
            try{
                const serverTimes = await serverTimeService.getAll();
                    res.status(200).send(serverTimes);
                   
                
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      
    
}
const deleteServerTimeLogs = async(req,res,next)=>{
            try{
                const serverTimes = await serverTimeService.deleteAll();
                     res.status(200).send({message:"All server time logs have been succesfully deleated!"});
                
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
      
    
}
///////////////////////////////////////////////////////////////

module.exports = {
    
    createJuryMember,
    getAllJuryMembers,
    getJuryMembersByProjectId,
    getUserByEmailAndPassword,
    updateJuryGrade,
    getMaxGrade,
    getMinGrade,
    getSumOfGrades,
    getNumberOfJuryMembers,
    selectTeamUserToJury,
    getJuryMemberByUserId,
    
    createPhase,
    getAllPhases,
    getPhasesByDeadline,
    getPhasesByProjectId,
    updatePhaseLink,
    updatePhaseDeadline,
    deletePhaseAndUpdateProject,
    
    createProjectWithPhases,
    createProject,
    getAllProjects,
    getProjectByDeadline,
    getProjectById,
    updateProjectFinalGrade,
    setFinalGrade,
    getProjectsByDeadline,
    
    createUser,
    getAllUsers,
    getUserByRole,
    getUserById,
    getUserByEmail,
    createTeam,
    getAllTeams,
    getTeamMembersByTeamName,
    getFinalTeamGrades,
    updateTeamSetProjectId,
    selectPossibleJuryMembersForProjectX,
    getTeamByProjectId,
    
    getServerTime,
    getServerTimeLogs,
    deleteServerTimeLogs,
    selectTeamUserToJury,
}