const express = require('express');
const router = express.Router();

const 
{   createUser, getAllUsers, getUserByRole,getUserById,getUserByEmailAndPassword,getUserByEmail,
    createJuryMember, getAllJuryMembers,getJuryMembersByProjectId,getMaxGrade,getMinGrade,getSumOfGrades,getNumberOfJuryMembers,
    createPhase, getAllPhases, getPhasesByDeadline,getPhasesByProjectId,updatePhaseLink,updatePhaseDeadline,deletePhaseAndUpdateProject,
    createProjectWithPhases,getAllProjects, getProjectByDeadline,getProjectById,getFinalTeamGrades,updateProjectFinalGrade,setFinalGrade,
    createTeam,getAllTeams,getTeamMembersByTeamName,updateJuryGrade,updateTeamSetProjectId,selectPossibleJuryMembersForProjectX,
    selectTeamUserToJury,getProjectsByDeadline,
    getServerTime,getServerTimeLogs,deleteServerTimeLogs,
    
    getJuryMemberByUserId,getTeamByProjectId,
}=require('./../controller/allControllers');

///////////////////////////////////////////////////////
///USER/////////////////////////////////////////////////
///////////////////////////////////////////////////////

//creaza un user
router.post('/user', createUser);

//returneaza un array de useri
router.get('/users', getAllUsers);

//retuneaza un obiect de tip user
router.get('/users/id/:id', getUserById);

//returneaza un array de obiecte
router.get('/users/role/:role', getUserByRole);

//returneaza un obiect de tip user sau un messaj-eroare
router.get('/users/email/:email/password/:password', getUserByEmailAndPassword);

//returneaza un obiect de tip user sau un messaj-eroare
router.get('/users/email/:email/', getUserByEmail);

///////////////////////////////////////////////////////
///Jury members////////////////////////////////////////
///////////////////////////////////////////////////////

//creaza un jury member
router.post('/jury-member', createJuryMember);

//returneaza un array de jury-members
router.get('/jury-members', getAllJuryMembers);

//returneaza un array de jury-memeber pentru un proiect (un juiu jurizeaza un singur proiect)
router.get('/jury-members/id/:id', getJuryMembersByProjectId);

//updateaza nota unui jurat pe un proiect
router.put('/jury-member/id/:id/project/:FKprojectId/grade/:grade', updateJuryGrade);



//@@@@
//returneaza un jury member dupa userId
router.get('/jury-members/user-id/:userId', getJuryMemberByUserId);

//returneaza cea mai mare nota acordata unui proiect
router.get('/jury-members/project/max/id/:projectId', getMaxGrade);

//returneaza cea mai mica nota acordata unui proiect
router.get('/jury-members/project/min/id/:projectId', getMinGrade);

//returneaza numarul de membrii ai juriului
router.get('/jury-members/number/project/id/:projectId', getNumberOfJuryMembers);

//returns the sum of phase grades 
router.get('/project/id/:projectId/sum', getSumOfGrades);



//insereaza un jury-member pentru proiectu x
router.post('/jury-member/selected/project-id/:FKprojectId', selectTeamUserToJury);
// ///
// var CronJob = require('cron').CronJob;
// new CronJob('* * * *  * *', function() {
    
//   console.log("Router cron");
// }, null, true, 'Europe/Bucharest');
// //

///////////////////////////////////////////////////////
///Phase//////////////////////////////////////////////
//////////////////////////////////////////////////////
//creaza o faza
router.post('/phase', createPhase);

//returneaza un array de faze
router.get('/phases', getAllPhases);

//retuneaza un array toate fazele care au un anumit deadline
router.get('/phases/deadline/:deadline', getPhasesByDeadline);

//returneaza un array de faze cu toate fazele dupa id proiectului
router.get('/phases/id/:id', getPhasesByProjectId);

//updateaza link-ul al unei faza, al unui proiect
router.put('/phases/project/:FKprojectId/id/:id/link', updatePhaseLink);

//updateaza deadline-ul al unei faza, al unui proiect
router.put('/phases/project/:FKprojectId/id/:id/deadline/:deadline', updatePhaseDeadline);


router.delete('/phases/delete/project/:FKprojectId/deadline/:deadline', deletePhaseAndUpdateProject);

/////////////////////////////////////////////////////////
///Project//////////////////////////////////////////////
////////////////////////////////////////////////////////

// router.post('/project', createProject);
//creeaza un proiect cu toate fazele specificate adaugand la fiecare faza = finalDeadline
router.post('/project/teamName/:teamName', createProjectWithPhases);

//returneaza un array de projects
router.get('/projects', getAllProjects);
//returneaza un  project care a ajuns la deadline
router.get('/project/id/:id/finalDeadline/:finalDeadline', getProjectByDeadline);

//returneaza un array de projects
router.get('/projects/finalDeadline/:finalDeadline', getProjectsByDeadline);
//returneaza un obiect de tip project
router.get('/projects/id/:id', getProjectById);


//returneaza un array de grades of project
router.get('/projects/grades', getFinalTeamGrades);


//updateaza nota finala a unui proiect, selectat prin id
router.put('/project/id/:id/grade/:finalGrade', updateProjectFinalGrade);

//update nota finala a unui proiect cu cea CALCULATA
router.put('/project/id/:projectId/finalgrade/', setFinalGrade);

/////////////////////////////////////////////////////////
///Team/////////////////////////////////////////////////
////////////////////////////////////////////////////////


//creaza un Team
router.post('/team',createTeam);
//returneaza un array de Team
router.get('/teams',getAllTeams);

//returneaza un array de Team dupa nume
router.get('/teams/teamName/:teamName',getTeamMembersByTeamName);

//seteaza project id din care fac parte toti membrii ai aceeasi echipe
router.put('/teams/teamName/:teamName/project/:FKprojectId',updateTeamSetProjectId);

//returneaza un obiect de tip Team care poate fi selectat pt a fi juriu
router.get('/teams/project/:FKprojectId',selectPossibleJuryMembersForProjectX);



//returneaza un obiect de tip Team care poate fi selectat pt a fi juriu
//@@@
router.get('/team/project-id/:FKprojectId',getTeamByProjectId);


/////////////////////////////////////////////////////////////////
///Server Stuff/////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

//returneaza timpul serverului
router.get('/time',getServerTime);


//returneaza all time logs ale serverului
router.get('/times',getServerTimeLogs);

deleteServerTimeLogs
router.delete('/times/delete',deleteServerTimeLogs);
module.exports = router;
