const configuration  = require('./../config/configuration.json');
const Sequelize = require('sequelize');

const DB_NAME = configuration.database.database_name;
const DB_USER = configuration.database.username;
const DB_PASS = configuration.database.password;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    dialect: 'mysql'
});


sequelize.authenticate().then(() => {
  
    console.log('Database connection success!');

}).catch(err => {
    console.log(`Database connection error: ${err}`);
});


class User extends Sequelize.Model { };

User.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    role:{
        type:Sequelize.STRING,
        allowNull:false,
        isAlpha: true
    },
    email:{
        type:Sequelize.STRING,
        allowNull:false,
         isEmail: true,
         unique: true
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
    }
    
}, {
    sequelize,
    modelName: 'users',
    timestamps: false
});


class Jury extends Sequelize.Model { };

Jury.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    grade:{
        type: Sequelize.FLOAT,
        allowNull: true,
        isNumeric: true,
        defaultValue: 0,
        max:10
          
    }
    
}, {
    sequelize,
    modelName: 'juries'
});



class Phase extends Sequelize.Model { };

Phase.init({
    id:{ 
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    deadline:{
        type:Sequelize.DATE,
        allowNull:false,
        isDate:true
    },
    link:{
        type: Sequelize.STRING,
        allowNull: true,
        isUrl: true
    },
    phaseNumber:{
        type: Sequelize.INTEGER,
        allowNull: true,
        isNumeric: true,
        defaultValue: 0,
        max:5
    }

}, {
    sequelize,
    modelName: 'phases'
});


class Project extends Sequelize.Model { };

Project.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    theme:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    finalGrade:{
        type:Sequelize.FLOAT,
        allowNull:true,
        isNumeric: true,
        defaultValue: 0,
        min:1,
        max:10
    },
    finalDeadline:{
        type:Sequelize.DATE,
        allowNull:false,
        isDate:true,
    },
    noPhases:{
        type: Sequelize.INTEGER,
        allowNull:false,
    }
}, {
    sequelize,
    modelName: 'projects'
});


class Team extends Sequelize.Model { };

Team.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    teamName:{ 
        type:Sequelize.STRING,
    }
}, {
    sequelize,
    modelName: 'teams',
    timestamps: false
});


class ServerTime extends Sequelize.Model { };

ServerTime.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    currentTime:{
        type:Sequelize.DATE,
        defaultValue:Sequelize.NOW
    }
},{
    sequelize,
    modelName: 'times',
    timestamps: false
});


ServerTime.sync({force:true});


////*****************POPULATING DB***********************************************////
// logica din spate e urmatoarea:
// ca sa putem sterge tabelele fara erori, intai stergem tabelele ce contin referinte catre alte tabele
// Ordinea e cam asa :
// Stergem : Phase,Team,Project,Jury,User
// Apoi cream: User,Team,Project,Jury,Phase
// Si dupa fiecare creeare populam cate o tabela, ca ulterior sa le putem construi si pe celelalte din mers
// toata treaba asta o reusit prin .then(function(){}) care practic le forteaza sa se apeleze in ordine



////*******************************RELATIONSHIPS*********************************////

User.hasMany(Jury,{foreignKey: { allowNull: true, name:'FKuserId'}, onDelete: 'CASCADE',constraints:false });
User.hasMany(Team,{foreignKey: { allowNull: true, name:'FKuserId' }, onDelete: 'CASCADE',constraints:false });
Project.hasOne(Team,{foreignKey:{ allowNull: true , name:'FKprojectId'}, /*onDelete: 'CASCADE',*/ constraints:false });

Project.hasOne(Jury,{foreignKey: { allowNull: true, name:'FKprojectId' }, /*onDelete: 'CASCADE',*/ constraints:false });
Project.hasMany(Phase,{foreignKey: { allowNull: true, name:'FKprojectId' }, onDelete: 'CASCADE',constraints:false });

////*****************************************************************************////

//Create and Populating the tables
Phase.drop({force:true}).then(function(){
    Team.drop({force:true}).then(function(){
        Project.drop({force:true}).then(function(){
            Jury.drop({force:true}).then(function(){
                User.drop({force:true}).then(function(){
                    User.sync({force:true}).then(function(){
                            User.create({"id":1,"role":"student","email":"ceva1@gmail.com","password":"etc1"});
                            User.create({"id":2,"role":"student","email":"ceva2@gmail.com","password":"etc2"});
                        	User.create({"id":3,"role":"student","email":"ceva3@gmail.com","password":"etc3"});
                            User.create({"id":4,"role":"teacher","email":"ceva4@gmail.com","password":"etc4"});
                            User.create({"id":5,"role":"teacher","email":"ceva5@gmail.com","password":"etc5"});
                            
                        Team.sync({force:true}).then(function(){
                                
                               
                                
                                 Team.create({"id":1,"teamName":"racheta","FKuserId":1,"FKprojectId":1});
                                 Team.create({"id":2,"teamName":"banana","FKuserId":2,"FKprojectId":2});
                                  Team.create({"id":3,"teamName":"racheta","FKuserId":3,"FKprojectId":1});
                                  Team.create({"id":4,"teamName":"racheta","FKuserId":4,"FKprojectId":1});
                                  Team.create({"id":5,"teamName":"test","FKuserId":5,"FKprojectId":2});
                                 //ca sa afli toti userii dintr-o echipa faci
                                //select * from teams where teamName='racheta';
                            Project.sync({force:true}).then(function(){
                                 
                                 
                                
                                 Project.create({"id":1,"theme":"Ceva","finalGrade":9.6,
                                	"finalDeadline":"2019-08-09 04:05:02","noPhases":5});
                                	
                                	Project.create({"id":2,"theme":"Something 2","finalGrade":3.7,
                                	"finalDeadline":"2019-08-09 04:05:02","noPhases":2});
                                
                                // Project.create({"id":2,"theme":"Something 2","finalGrade":3.7,
                                // 	"finalDeadline":"2020-01-10","noPhases":2});
                                	
                                	Project.create({"id":3,"theme":"Something 3","finalGrade":7.7,
                                	"finalDeadline":"2018-08-09 04:05:02","noPhases":9});
                                	Project.create({"id":4,"theme":"Something 5",
                                	"finalDeadline":"2018-08-09 04:05:02","noPhases":19});
                                	 //select distinct t.teamName, p.finalGrade from teams t, projects p where p.id=t.FKprojectId;
                                //nota finala pe care o primeste o echipa
                                 Jury.sync({force:true}).then(function(){
                                     
                                    //     Jury.create({"id":1, "grade":8,"FKuserId":"1","FKprojectId":2});
                                    //     Jury.create({"id":2, "grade":5.6,"FKuserId":"2","FKprojectId":1});
                                    // 	Jury.create({"id":3, "grade":8.4,"FKuserId":"4","FKprojectId":2});
                                    // 	Jury.create({"id":4, "grade":7.4,"FKuserId":"3","FKprojectId":2});
                                    	//select grade from juries  where FKprojectId=2;
                                    	//toate notele juratilor pentru proiectul cu id 2
                                        Phase.sync({force:true}).then(function(){
                                            
                                             Phase.create({"id":1,"deadline":"2019-08-09","link":"URL://link3.org","FKprojectId":1, "phaseNumber":1});
                                             Phase.create({"id":2,"deadline":"2019-08-09","link":"URL://link3.org","FKprojectId":1,  "phaseNumber":2});
                                             Phase.create({"id":3,"deadline":"2019-08-19","link":"URL://link3.org","FKprojectId":1,  "phaseNumber":3});
                                             Phase.create({"id":4,"deadline":"2019-08-19","link":"URL://link3.org","FKprojectId":1,  "phaseNumber":4});
                                             Phase.create({"id":5,"deadline":"2019-08-29 11:07:02","link":"URL://link3.org","FKprojectId":1,  "phaseNumber":5});
                                             Phase.create({"id":6,"deadline":"2019-08-19","link":"URL://link3.org","FKprojectId":2,  "phaseNumber":1});
                                             Phase.create({"id":7,"deadline":"2019-08-29 11:07:02","link":"URL://link3.org","FKprojectId":2,  "phaseNumber":2});
                                        
                                        
                                            //nota pentru fiecare faza a turor proiectelor ordonate dupa ID-ul proiectului(FKprojectId)
                                            //select FKprojectId,grade from phases order  by FKprojectId;  
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})
///@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// Phase.drop({force:true}).then(function(){
//     Team.drop({force:true}).then(function(){
//         Project.drop({force:true}).then(function(){
//             Jury.drop({force:true}).then(function(){
//                 User.drop({force:true}).then(function(){
//                     User.sync({force:true}).then(function(){
//                         Team.sync({force:true}).then(function(){
//                                  //ca sa afli toti userii dintr-o echipa faci
//                                 //select * from teams where teamName='racheta';
//                             Project.sync({force:true}).then(function(){
//                                 	 //select distinct t.teamName, p.finalGrade from teams t, projects p where p.id=t.FKprojectId;
//                                 //nota finala pe care o primeste o echipa
//                                  Jury.sync({force:true}).then(function(){
//                                     	//select grade from juries  where FKprojectId=2;
//                                     	//toate notele juratilor pentru proiectul cu id 2
//                                         Phase.sync({force:true}).then(function(){
                                            
                                          
//                                             //nota pentru fiecare faza a turor proiectelor ordonate dupa ID-ul proiectului(FKprojectId)
//                                             //select FKprojectId,grade from phases order  by FKprojectId;  
//                                     })
//                                 })
//                             })
//                         })
//                     })
//                 })
//             })
//         })
//     })
// })











////*****************************************************************************////


////************************CUSTOM RAW QUERRIES***********************************////



////******************************************************************************////

module.exports = {
    sequelize,
    Jury,
    User,
    Phase,
    Project,
    Team,
    ServerTime
    
}