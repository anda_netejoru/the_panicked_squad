 const { Jury,User,Phase,Project,Team,ServerTime } = require('./../models/allModels');

//const sequelize = require('sequelize');

const { sequelize }= require('./../models/allModels');

const juryService = {
    create: async (jury) => {
        try {
            const result = await Jury.create(jury);
            return result;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const juryMembers = await Jury.findAll();
            return juryMembers;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getJuryMembersByUserId: async (id) => {
        try {
            const juryMembers = await Jury.findOne({where:{FKuserId: id}});
            return juryMembers;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getJuryMembersByProjectId: async (id) => {
        try {
            const juryMembers = await Jury.findAll({where:{FKprojectId: id}});
            return juryMembers;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    updateGrade: async(juryId, projectId ,grade) => {
        try{
            const jury = await Jury.findOne({where:{FKuserId: juryId, FKprojectId: projectId}});
            jury.update({grade: grade});
            return jury;
        } catch(err){
            throw new Error(err.message);
        }
    },
    getMaxGrade: async (projectId)=>{
        try{
              const maxGrade = Jury.findAll({
                                        where:{FKprojectId : projectId},
                                        attributes:[[sequelize.fn('max', sequelize.col('grade')),'max']] //???????????????????//
                                    })
            return maxGrade;                        
        }
         catch(err) {
            throw new Error(err.message);
        }
    },
    getMinGrade: async (projectId)=>{
        try{
              const minGrade = Jury.findAll({
                                        where:{FKprojectId : projectId},
                                        attributes:[[sequelize.fn('min', sequelize.col('grade')),'min']]
                                    });
                return minGrade;
        }
         catch(err) {
            throw new Error(err.message);
        }
    },
    getSumOfGrades: async(projectId) => {
        try{
            const sum=Jury.findAll({
                    where:{FKprojectId : projectId},
                    attributes:[[sequelize.fn('sum', sequelize.col('grade')),'sumOfGrades']]
                    });
            return sum;
        }
        catch(err){
            throw new Error(err.message);
        }
    },
    getNumberOfJuryMembers:async(projectId) => {
        try{
            const numberOfMembers=Jury.findAll({
                    where:{FKprojectId : projectId},
                    attributes:[[sequelize.fn('count', sequelize.col('id')),'NoOfMembers']]
                    });
            return numberOfMembers;
        }
        catch(err){
            throw new Error(err.message);
        }
    },
    //@@
    getJuryMemberByUserId:async(userId) => {
        try{
            const juryMember=Jury.findOne({
                    where:{FKuserId : userId}
                    
                    });
            return juryMember;
        }
        catch(err){
            throw new Error(err.message);
        }
    },    
    
}
 
const serverTimeService={
    create: async () => {
        try {
            const currentTime = await ServerTime.create();
            return currentTime;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll:async () => {
        try {
           
             const currentTimes = await ServerTime.findAll();
            
            return currentTimes;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    deleteAll:async () => {
        try {
            const currentTimes = await ServerTime.destroy({where: {}})
            return currentTimes;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    
}


const phaseService = {
    create: async (phase) => {
        try {
            const result = await Phase.create(phase);
            return result;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const phases = await Phase.findAll();
            return phases;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getPhasesByDeadline: async (deadline) => {
        try {
            const phases = await Phase.findAll({where:{deadline: deadline}});
            return phases;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getPhasesByProjectId: async (id) => {
        try {
            const phases = await Phase.findAll({where:{FKprojectId: id}});
            return phases;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    updatePhaseLink: async (idPhase, idProject, link) => { 
        try {
            const phase = await Phase.findOne({where:{phaseNumber:idPhase,FKprojectId:idProject}});
            phase.update({link:link})
          
            return phase;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    updatePhaseDeadline:async (idPhase, idProject, deadline) => { 
        try {
            const phase = await Phase.findOne({where:{phaseNumber:idPhase,FKprojectId:idProject}});
           if(phase)
           {
                phase.update({deadline:deadline})
                return phase;
           }
         
           
         
        } catch(err) {
            throw new Error(err.message);
        }
    },
    deletePhaseAndUpdateProject: async (projectId, deadline) => {
        try {
            const phase = await Phase.destroy({where:{FKprojectId: projectId, deadline: deadline}});
            const project = await Project.findByPk(projectId);
            
            project.update({noPhases:project.noPhases-1});
            return project;
        } catch(err) {
            throw new Error(err.message);
        }
    }
}


const projectService = {
    create: async (project) => {
        try {
            const result = await Project.create(project);
            return result;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const projects = await Project.findAll();
            return projects;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getProjectByDeadline: async (finalDeadline,projectId) => {
        try {
            const project = await Project.findOne({where:{finalDeadline: finalDeadline,id:projectId}});
            return project;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getProjectsByDeadline: async (finalDeadline) => {
        try {
            const project = await Project.findAll({where:{finalDeadline: finalDeadline}});
            return project;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getProjectById: async (id) => {
        try {
            const project = await Project.findByPk(id);
            return project;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getFinalTeamGrades: async()=>{
            try {
                const projects = await Project.findAll({
                  include: [{
                    model: Team,
                    required: true
                   }]
                });
                
                return projects;
            }catch(err) {
            throw new Error(err.message);
        }
    },
    updateProjectFinalGrade: async (id, grade) => {
        try {
            const project = await Project.findByPk(id);
            project.update({finalGrade: grade});
            return project;
        }
        catch(err) {
            throw new Error(err.message);
        }
    },
    setFinalGrade:  async (projectId) => {
        try {
        let maxGrade = await Jury.findAll({
        where:{FKprojectId : projectId},
        attributes:[[sequelize.fn('max', sequelize.col('grade')),'MAX']] 
        });
        let minGrade = await Jury.findAll({
        where:{FKprojectId : projectId},
        attributes:[[sequelize.fn('min', sequelize.col('grade')),'MIN']]
        });
        let sum=await Jury.findAll({
        where:{FKprojectId : projectId},
        attributes:[[sequelize.fn('sum', sequelize.col('grade')),'SUMOFGRADES']]
        });
        let numberOfMembers=await Jury.findAll({
        where:{FKprojectId : projectId},
        attributes:[[sequelize.fn('count', sequelize.col('id')),'NOM']]
    
        });
        

        let no=numberOfMembers[0].get('NOM');
        let suma=sum[0].get('SUMOFGRADES');
        let min=minGrade[0].get('MIN');
        let max=maxGrade[0].get('MAX');
        
         const project = await Project.findByPk(projectId);
        if(no>2)
        {
        
            suma-=min;
            suma-=max;
            no-=2;
            let result =(suma/no);
            project.update({finalGrade:result });
            return project;
        }
        return project;
            
        }
        catch(err) {
            throw new Error(err.message);
        }
    },
}

const userService = {
    create: async (user) => {
        try {

             const result=await User.create(user);
            return result;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const users = await User.findAll();
            return users;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getUserByRole: async (role) => {
        try {
            const users = await User.findAll({where:{role: role}});
            return users;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getUserById: async (id) => {
        try {
            const users = await User.findByPk(id);
            return users;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getUserByEmailAndPassword: async(email, password) => {
        try{
            const user = await User.findOne({where:{email:email, password:password}});
            return user;
        } catch(err){
            throw new Error(err.message);
        }
    },
    getUserByEmail:async (email) => {
        try {
            const user = await User.findOne({where:{email: email}});
            return user;
        } catch(err) {
            throw new Error(err.message);
        }
    }
}


const teamService= {
    create: async (team) => {
        try {
            const result = await Team.create(team);
            return result;    
        } catch(err) {
          throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const result = await Team.findAll();
            return result;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getTeamMembersByTeamName: async (teamName) => {
        try {
            const teamMembers = await Team.findAll({where:{teamName: teamName}});
            return teamMembers;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getTeamByProjectId: async (projectId) => {
        try {
            const teamMember = await Team.findOne({where:{FKprojectId: projectId}});
            return teamMember;
        } catch(err) {
            throw new Error(err.message);
        }
    },    
    updateTeamSetProjectId: async (teamName, projectId) => {
        try {
            const teams = await Team.update({FKprojectId: projectId}, {where:{teamName: teamName}});
            return teams;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    selectPossibleJuryMemberForProjectX: async (projectId) => {
        try {
            const result= await sequelize.query(`select distinct t.* from phases ph, teams t  where t.FKprojectId!=${projectId} order by RAND() limit 1;`, 
                     { replacements: { status: 'active' }, type: sequelize.QueryTypes.SELECT }
            );
            return result[0];
            
        } catch(err) {
            throw new Error(err.message);
        }
    },
    selectAllPossibleJuryMembersForProjectX: async (projectId) => {
        try {
            const result= await sequelize.query(`select distinct t.* from phases ph, teams t  where t.FKprojectId!=${projectId};`, 
                     { replacements: { status: 'active' }, type: sequelize.QueryTypes.SELECT }
            );
            return result;
            
        } catch(err) {
            throw new Error(err.message);
        }
    }
}
module.exports = {
    juryService,
    phaseService,
    userService,
    projectService,
    teamService,
    serverTimeService
    
};