import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './CreateProject.css';
const HostIp=process.env.REACT_APP_IP;

class CreateProject extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            role: 'teacher',
             //grades: [],
             clicked:false,
             theme: '',
             projectDeadline: '',
             noPhases: '',
             phaseDeadline:'',
             phaseNumber:1,
             teamName:'',
             project:
            {   
                "team":'',
                "finalGrade":null,
                "finalDeadline" : "",
                "noPhases":''
            },
            teamMembers:[]
            // projectId:5
        };
       
    }
    
       handleChangePhaseNumber = (event) => {
         
        this.setState({
            phaseNumber: event.target.value,
        });
        console.log(this.state.phaseNumber);
    }
    
     handleChangeTheme = (event) => {
         
        this.setState({
            theme: event.target.value,
        });
        console.log(this.state.theme);
    }
     handleChangenoPhases = (event) => {
         
        this.setState({
            noPhases: event.target.value,
        });
      console.log(this.state.noPhases);
    }
    
     handleChangePhaseDeadline = (event) => {
         
        this.setState({
            phaseDeadline: event.target.value,
        });
         console.log(this.state.phaseDeadline);
    }

    

    
    handleChangeDeadline = (event) => {
        this.setState({
            projectDeadline: event.target.value,
        });
        console.log(this.state.projectDeadline);
    }
    
   
 goToHome=()=>{
  window.location=`http://${HostIp}:3000/#/home-student`;
}

componentDidMount=()=>{
    this.setState({
        teamName:this.props.receiveTeam.teamName
    })
}

handleCreateProjectWithPhases=()=>{
    
    console.log(this.state.teamName, this.state.projectDeadline, this.state.theme, this.state.noPhases);
    
    let project=this.state.project; 
    project.theme = this.state.theme;
    project.noPhases = this.state.noPhases;
    project.finalDeadline = this.state.projectDeadline;

    
    axios.post(`http://${HostIp}:8080/api/project/teamName/${this.state.teamName}`, project)
              .then(res => {
                //trebuie sa fac aici get project by theme si sa setez id-ul proiectului aici, ca sa pot apoi sa fac update pe phases la link cu project id si phase id
                //this.state.projectId = res.data.id;
                //console.log(res.data.id);
             }).then(()=>{
        axios.get(`http://${HostIp}:8080/api/teams/teamName/${this.state.teamName}`).then(res => {
         this.setState({
            teamMembers:res.data
        })
          console.log("teamMembers",this.state.teamMembers)
          
         this.setState({ project: { ...this.state.project, id: this.state.teamMembers[0].FKprojectId} });
         this.setState({ project: { ...this.state.project, team: this.state.teamMembers[0].teamName} });  
         console.log("Updated Project",this.state.project);
         alert("Project created!");
        }).catch(err => {
            console.log(err);
            alert("No user belongs to this team !")
        })
});


this.setState({clicked:!this.setState.clicked})
   
}

 handleUpdatePhaseDeadline = (event) => {
    console.log(this.state.phaseNumber, this.state.phaseDeadline);
    axios.put(`http://${HostIp}:8080/api/phases/project/${this.state.project.id}/id/${this.state.phaseNumber}/deadline/${this.state.phaseDeadline}`)
              .then(res => {
                console.log(res);
                console.log(res.data);
                alert("Phase deadline set!");
             }).catch(err => {
            console.log(err);
           alert("Error setting deadline")
        })
    
    }
  
    
    render(){
        if(this.state.clicked)
        {
              return(
            
        <div>
         {/*this.props.receiveTeam.teamName*/}
            <br/>
            <div id="home-teacher-title">
            <h1>Create Project</h1>
            
            </div>
            <br/>
            <br/>
               <Button label="Home" onClick={this.goToHome} />
                <br/>
                <br/>
            <div id='add-project-layout'>
             <span className="p-float-label">
                <InputText id="idTheme" value={this.state.theme} onChange={this.handleChangeTheme} />
                <label htmlFor="in">Theme</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <span className="p-float-label">
                <InputText id="idNoPhases" value={this.state.noPhases} onChange={this.handleChangenoPhases} />
                <label htmlFor="in">Number of phases</label>
                <br/>
                <br/>
                <br/>
            </span>
             <span className="p-float-label">
                <InputText id="idDeadline" value={this.state.projectDeadline} onChange={this.handleChangeDeadline} />
                <label htmlFor="in">Deadline (YYYY-MM-DD)</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
           
             <Button label="Create Project" onClick={this.handleCreateProjectWithPhases} />
         <br/>
            <br/>
            <br/>
            <br/>
        
        </div>
        <h3>Note: You can only have at most 5 phases. The last phase you set is the final deadline.</h3>
        <div id='add-project-layout-2'>
 
        <span className="p-float-label">
            <InputText id="idphaseNumber" value={this.state.phaseNumber} onChange={this.handleChangePhaseNumber} />
            <label htmlFor="in">Phase number</label>
            <br/>
            <br/>
            <br/>
            <br/>
        </span>

              <span className="p-float-label">
            <InputText id="idPhaseDeadline" value={this.state.phaseDeadline} onChange={this.handleChangePhaseDeadline} />
            <label htmlFor="in">Phase deadline (YYYY-MM-DD)</label>
            <br/>
            <br/>
            <br/>
            <br/>
        </span>
        
         <Button label="Update phase deadline" onClick={this.handleUpdatePhaseDeadline} />

        
        </div>    
        </div>
        ); 
        }
        else
        {
                     return(
            
        <div>
         {/*this.props.receiveTeam.teamName*/}
            <br/>
            <div id="home-teacher-title">
            <h1>Create Project</h1>
            
            </div>
            <br/>
            <br/>
               <Button label="Home" onClick={this.goToHome} />
                <br/>
                <br/>
            <div id='add-project-layout'>
             <span className="p-float-label">
                <InputText id="idTheme" value={this.state.theme} onChange={this.handleChangeTheme} />
                <label htmlFor="in">Theme</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <span className="p-float-label">
                <InputText id="idNoPhases" value={this.state.noPhases} onChange={this.handleChangenoPhases} />
                <label htmlFor="in">Number of phases</label>
                <br/>
                <br/>
                <br/>
            </span>
             <span className="p-float-label">
                <InputText id="idDeadline" value={this.state.projectDeadline} onChange={this.handleChangeDeadline} />
                <label htmlFor="in">Deadline (YYYY-MM-DD)</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
           
             <Button label="Create Project" onClick={this.handleCreateProjectWithPhases} />
         <br/>
            <br/>
            <br/>
            <br/>
        
        </div>
        <h3>Note: You can only have at most 5 phases. The last phase you set is the final deadline.</h3>
   
        </div>
        );  
        }
      
        
    }
}

export default withRouter(CreateProject);