import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './CreateTeam.css';
const HostIp=process.env.REACT_APP_IP;
class CreateTeam extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            email: '',
            teamName:'',
            clicked:false,
            teamMembers:[{email:''}],
            team:
            {  
                "teamName":'',
                 "FKuserId":null,
                "FKprojectId":null
            }
            
        };
       
    }
     handleChangeEmail = (event) => {
         
        this.setState({
            email: event.target.value,
           
        });
       // console.log(this.state.email);
    }
    handleChangeteamName = (event) => {
       
          const click=this.state.clicked;
          if(click===false)
          {
          this.setState({clicked: !this.state.clicked});
          }
    this.setState({ team: { ...this.state.team, teamName: event.target.value} });
    
      
    }

    handleClick=()=>{

        //creez echipa si ma adauc pe mine
        if(this.state.clicked)
        { //am creat echipa ..yey
            let team2=this.state.team; 
            
            console.log(this.props.loggedUser);
            //ma dau pe mine
            team2.FKuserId=this.props.loggedUser.id
            
             this.props.sendTeam(this.state.team);
            axios.post(`http://${HostIp}:8080/api/team`,team2)
              .then(res => {
                console.log(res);
                console.log(res.data);
                alert("Team created!");
             }).catch(err => {
            console.log(err);
           alert("Error creating team")
        })
             
          
        } 
        
        
        
    }
    HandleAdd=()=>{
        if(this.state.clicked)
        {
            let team=this.state.team; 
           
             axios.get(`http://${HostIp}:8080/api/users/email/${this.state.email}`).then(res => {
                    const userID=res.data.id;
                    team.FKuserId=userID;
                    //am creat echipa ..yey
                    axios.post(`http://${HostIp}:8080/api/team`,team)
                      .then(res => {
                        console.log(res);
                        console.log(res.data);
                        alert("Team member added!");
                        
                     });
        }).catch(err => {
            console.log(err);
           alert("Unregistered email!")
        })
        } 
            
        }
    
     goToHome=()=>{
  window.location=`http://${HostIp}:3000/#/home-student`;
}
    render(){
      //  console.log(this.props.loggedUser);
        if(this.state.clicked===true)
        {
         const teamMembers = this.state.teamMembers.map((teamMembers,index) => 
                <div id={`team-member${index}`} key={`team-member${index}`}>
                <span className="p-float-label">
                    <InputText id={`idTeamMember${index}`} value={this.state.email||""} onChange={this.handleChangeEmail} />
                    
                    <label htmlFor={`idTeamMember${index}`}>Member email </label>
                 </span>
            <Button id={`btnAddTeamMember${index}`} label="Add" onClick={this.HandleAdd}/>
             
                </div>
         
             );    
            return(
            <div>
             Create Team
              {this.props.loggedUser.email}
             <br/>
             <br/>
              <br/>
               <br/>
             <div className="team-create-div">
           <span className="p-float-label">
                    <InputText id="idTeamName" value={this.state.team.teamName} onChange={this.handleChangeteamName} />
                    <label htmlFor="idTeamName">Team name</label>
                 </span>
            <Button id='btnCreateTeam' label="Create" onClick={this.handleClick}/>
            </div>
            {teamMembers}
            <br/>
            <br/>
              <Button label="Home" onClick={this.goToHome} />
            </div>
            );
        }
        
        else
        {
        return(
        <div>
         Create Team
         <br/>
          {this.props.loggedUser.email}
         <br/>
          <br/>
           <br/>
         <div className="team-create-div">
       <span className="p-float-label">
                <InputText id="idTeamName" value={this.state.teamName} onChange={this.handleChangeteamName} />
                <label htmlFor="idTeamName">Team name</label>
             </span>
        <Button id='btnCreateTeam' label="Create" onClick={this.handleClick}/>
        </div>
        <br/>
            <br/>
              <Button label="Home" onClick={this.goToHome} />
        </div>    
        ); 
        }
    
    }
}

export default withRouter(CreateTeam);