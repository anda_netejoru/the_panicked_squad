import React from 'react';
// const HostIp=process.env.REACT_APP_IP;

class UserList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
           
        };
    }
    
     componentDidUpdate = (prevProps) => {
        if(prevProps.users.length !== this.props.users.length) {
             this.setState({
            users: this.props.users
        })  
        }
    }
        render() {
        const users = this.state.users.map((user, index) => <div key={index}>{user.id}  {user.email}  {user.password} {user.role} </div>)
        return (
            <div>
            <br/>
            List of Users:
            <br/>
            <br/>
                {users}
            </div>
        );
    }
    
}
export default UserList;