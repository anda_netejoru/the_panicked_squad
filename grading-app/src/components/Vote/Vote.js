import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './Vote.css';
// import ReactVideoPlayer from '../ReactVideoPlayer/ReactVideoPlayer';
// import Moment from 'react-moment';
var moment = require('moment');
const HostIp=process.env.REACT_APP_IP;

class Vote extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
             clicked:false,
             teamName:'',
             theme:'',
             grade:'',
             projectId:'',
             userId:'',
             curTime:'',
             projectDeadline:'',
             phases:[]
        };
       
    }
    
    handleChangeTeamName = (event) => {
        // this.setState({
        //     teamName: event.target.value,
        // });
        
         this.setState({
            teamName: this.state.teamName,
        });
    }
    
    handleChangeTheme = (event) => {
        // this.setState({
        //     theme: event.target.value,
        // });
        
         this.setState({
            theme: this.state.theme,
        });
    }
    
    handleChangeGrade = (event) => {
        this.setState({
            grade: event.target.value,
        });
    }
    
  
handleVote=()=>{
    
    axios.put(`http://${HostIp}:8080/api/jury-member/id/${this.props.loggedUser.id}/project/${this.state.projectId}/grade/${this.state.grade}`)
              .then(res => {
                  console.log("grade updated successfully");
                  alert("Grade updated successfully!")
              }).catch(err => {
            console.log(err);
            alert("Error updating grade");
        })
}
              
    
  
 goToHome=()=>{
  window.location=`http://${HostIp}:3000/#/home-student`;
}
    
componentDidMount = ()=>{
    
    
    axios.get(`http://${HostIp}:8080/api/jury-members/user-id/${this.props.loggedUser.id}`).then(res=>{
        console.log("jury",res.data);
        this.setState({projectId:res.data.FKprojectId,grade:res.data.grade})
        
    }).catch(err=>{
       // console.log(err)
    }).then(()=>{
        axios.get(`http://${HostIp}:8080/api/team/project-id/${this.state.projectId}`).then(res=>{
        console.log("team",res.data);
        
       this.setState({
            teamName: res.data.teamName,
        });
        
        }).catch(err=>{
        //console.log(err)
        })
        
        
        axios.get(`http://${HostIp}:8080/api/projects/id/${this.state.projectId}`).then(res=>{
        console.log("project,",res.data);
        
       this.setState({
            theme: res.data.theme,
            projectDeadline:res.data.finalDeadline
        });
        
        }).catch(err=>{
       // console.log(err)
        }) 
         
        axios.get(`http://${HostIp}:8080/api/phases/id/${this.state.projectId}`).then(res=>{
            this.setState({phases:res.data});
            
        }).catch(err=>{
        //console.log(err)
        }) 
    })
  
  
  
}   

    
    render(){
  
        const showPhases=this.state.phases.map((p,index)=><div key={p.phaseNumber}> Phase: {p.phaseNumber} Link:  <a href={p.link}>{p.link}</a> <br/>  </div>)
       // console.log('data din consola',moment().add(30, 'days').format("YYYY-MM-DD"));
       console.log(this.state.phases);
       
        var currentDATE=moment().format('YYYY MM DD');
        console.log('current date',currentDATE);
       
        var maxDate=moment(this.state.projectDeadline.substring(0,10)).add(3, 'days').format('YYYY MM DD');
        console.log('current date2',maxDate);

    if(currentDATE<maxDate)
    {
         return(
        <div>
        {this.props.loggedUser.email} <br/>Project deadline: <br/>{this.state.projectDeadline}
            <br/>
            <div id="home-teacher-title">
            <h1>Vote</h1>
            </div>
            <br/>
            <br/>
               <Button label="Home" onClick={this.goToHome} />
                <br/>
                <br/>
            <br/>
            <br/>
             <div className='add-phase-layout'>
            <br/>
            <span className="p-float-label">
                <InputText id="idTeamName" value={this.state.teamName} onChange={this.handleChangeTeamName} />
                <label htmlFor="in">Team name</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <div id='space-div'></div>
            <span className="p-float-label">
                <InputText id="idTheme" value={this.state.theme} onChange={this.handleChangeTheme} />
                <label htmlFor="in">Theme</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <div id='space-div'></div>
            <span className="p-float-label">
                <InputText id="idGrade" value={this.state.grade} onChange={this.handleChangeGrade} />
                <label htmlFor="in">Grade</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
         
             <Button  id='btnSubmit' label="Vote" onClick={this.handleVote} />
            </div>
            
            <div id="center-video">
           { /*<ReactVideoPlayer  link='https://www.youtube.com/watch?v=FkOM7n2fbu0&feature=youtu.be'/>   */}
            
             
            </div>
            {showPhases}
           
        </div>
        ); 
    }
    else
    {
         return(
        <div>
        {this.props.loggedUser.email} <br/>Project deadline: <br/>{this.state.projectDeadline}
            <br/>
            <div id="home-teacher-title">
            <h1>Vote</h1>
            </div>
            <br/>
            <br/>
               <Button label="Home" onClick={this.goToHome} />
                <br/>
                <br/>
            <br/>
            <br/>
             <div className='add-phase-layout'>
            <br/>
            <span className="p-float-label">
                <InputText id="idTeamName" value={this.state.teamName} onChange={this.handleChangeTeamName} />
                <label htmlFor="in">Team name</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <div id='space-div'></div>
            <span className="p-float-label">
                <InputText id="idTheme" value={this.state.theme} onChange={this.handleChangeTheme} />
                <label htmlFor="in">Theme</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <div id='space-div'></div>
            <span className="p-float-label">
                <InputText id="idGrade" value={this.state.grade} onChange={this.handleChangeGrade} />
                <label htmlFor="in">Grade</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
           
            {/* <Button  id='btnSubmit' label="Vote" onClick={this.handleVote} />*/}
            </div>
            
            <div id="center-video">
           { /*<ReactVideoPlayer  link='https://www.youtube.com/watch?v=FkOM7n2fbu0&feature=youtu.be'/>   */}
           
            
            </div>
            {showPhases}
            
        </div>
        ); 
    }
    
    
        
    }
}

export default withRouter(Vote);