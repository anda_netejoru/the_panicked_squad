import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
const HostIp=process.env.REACT_APP_IP;
class AddUser extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            role: 'STUDENT',
            email: '',
            password:''
        };
       
    }
    
     handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value,
           
        });
    }
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }
    handleChangeRole = (event) => {
        this.setState({
            role: event.target.value
        });
    }
    
    addUser = () => {
        const user = this.state;
        axios.post(`http://${HostIp}:8080/api/user`, user).then(res => {
            this.props.onUserAdded(user);
           //postman message
            console.log(res.data);
            //actual data
             console.log(res.config.data);
        }).catch(err => {
            console.log(err);
        })
        // window.location.reload();
         //window.location=`http://${HostIp}:3000/#/login`;
    }
 goToLogin=()=>{
  window.location=`http://${HostIp}:3000/#/login`;
}
    
    render(){
    const roleOptions = [
    {label: 'student', value: 'STUDENT'},
    {label: 'teacher', value: 'TEACHER'},
];
        return(
        <div>
        <br/>
        
            <div className='add-user-layout'>
            Register
            <br/>
            <br/>

            <span className="p-float-label">
           
                <InputText id="idEmail" value={this.state.email} onChange={this.handleChangeEmail} />
                <label htmlFor="in">Email</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            
                <span className="p-float-label">
                <InputText id="idPassword" value={this.state.password} onChange={this.handleChangePassword} />
                <label htmlFor="idPassword">Password</label>
                <br/>
                <br/>
                <br/>
                <br/>
             </span>
    <SelectButton value={this.state.role} options={roleOptions} onChange={/*(e) => this.setState({role: e.value})*/ this.handleChangeRole}></SelectButton>
                <br/>
                <Button label="Register" onClick={this.addUser} />
                <br/>
                <br/>
                 <Button label="Go to login" onClick={this.goToLogin} />
            </div>
        </div>    
        );
    }
}

export default AddUser;