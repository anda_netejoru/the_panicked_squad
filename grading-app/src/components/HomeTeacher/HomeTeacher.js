import React from 'react';
import axios from 'axios';
// import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './HomeTeacher.css';
const HostIp=process.env.REACT_APP_IP;

class homeTeacher extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            role: 'teacher',
             grades: [],
             clicked:false,
        };
       
    }
    
  GetGrades=()=>{
      axios.get(`http://${HostIp}:8080/api/projects/grades`).then(res => {
            const gradesDB=res.data;
           this.setState({
          grades: gradesDB
        });
        }).catch(err => {
            console.log(err);
            alert("Error - no grades or sth!")
            //window.location=`http://${HostIp}:3000/#/error`;
            //return( <LoginUser render={()=>(<div>Wrong credentials</div>)} />);
           
            // console.log("No such user!!");
        
        })
        
        this.setState({
          clicked: !this.state.clicked
        });
  }  
   
 goToLogin=()=>{
  window.location=`http://${HostIp}:3000/#/login`;
}
    
    render(){
        const gra=this.state.grades;
        console.log(gra);
         const gradeToBeDisplayed = this.state.grades.map((grade, index) => <div key={index}>  {grade.team.teamName} {grade.finalGrade}</div>)
        if(this.state.clicked)
        {
            return(
        <div>
            <br/>
            <div id="home-teacher-title">
            <h1>Home</h1>
            
            </div>
            <br/>
            <br/>
               <Button label="See grades" onClick={this.GetGrades} />
               <br/>
               <br/>
             <Button label="Log out" onClick={this.goToLogin} />
            <br/>
            <br/>
            <h2>List of grades</h2>
               {gradeToBeDisplayed}
               
            
        </div>
      
        );
        }
        else
        {
           return(
        <div>
            <br/>
            <div id="home-teacher-title">
            <h1>Home</h1>
            
            </div>
            <br/>
            <br/>
               <Button label="See grades" onClick={this.GetGrades} />
                <br/>
                <br/>
             <Button label="Log out" onClick={this.goToLogin} />
            <br/>
            <br/>
            
        </div>
        ); 
        }
        
    }
}

export default withRouter(homeTeacher);