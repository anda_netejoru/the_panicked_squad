import React from 'react';
// import axios from 'axios';
// import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './HomeStudent.css';
const HostIp=process.env.REACT_APP_IP;

class homeStudent extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            role: 'teacher',
             grades: [],
             clicked:false,
             teamName:''
        };
       
    }
    
componentDidMount=()=>{
    this.setState({teamName:this.props.receiveTeam.teamName})
}    
    
//   GetGrades=()=>{
//       axios.get(`http://${HostIp}:8080/api/projects/grades`).then(res => {
//             const gradesDB=res.data;
//           this.setState({
//           grades: gradesDB
//         });
//         }).catch(err => {
//             console.log(err);
//             alert("Error - no grades or sth!")
//             //window.location=`http://${HostIp}:3000/#/error`;
//             //return( <LoginUser render={()=>(<div>Wrong credentials</div>)} />);
           
//             // console.log("No such user!!");
        
//         })
        
//         this.setState({
//           clicked: !this.state.clicked
//         });
//   }  
   
 goToCreateTeam=()=>{
  window.location=`http://${HostIp}:3000/#/home-student/create-team`;
}
goToCreateProject=()=>{
  window.location=`http://${HostIp}:3000/#/home-student/create-project`;
}
goToUploadPartialDeliverables=()=>{
  window.location=`http://${HostIp}:3000/#/home-student/upload-phase`;
}
goToVote=()=>{
  window.location=`http://${HostIp}:3000/#/home-student/vote`;
}
 goToLogin=()=>{
  window.location=`http://${HostIp}:3000/#/login`;
}
    
    render(){
        // const gra=this.state.grades;
        // console.log(gra);
        // const gradeToBeDisplayed = this.state.grades.map((grade, index) => <div key={index}>  {grade.team.teamName} {grade.finalGrade}</div>)
      
      //de modificat
            return(
        <div>
           <h1>Student Home</h1>
           <div id='dashboard'>
            <Button label="Create team" onClick={this.goToCreateTeam} />
            <br/>
            <Button label="Create project" onClick={this.goToCreateProject} />
            <br/>
            <Button label="Upload partial deliverable" onClick={this.goToUploadPartialDeliverables} />
            <br/>
            <Button label="Vote" onClick={this.goToVote} />
            <br/>
            <br/>
            <br/>
            <br/>
            <Button label="Log out" onClick={this.goToLogin} />
            </div>
        </div>
        );
      
      
      
    //   if(this.state.teamName)
    //   {
    //     return(
    //     <div>
    //       <h1>Student Home</h1>
    //       <div id='dashboard'>
            
    //         <Button label="Create project" onClick={this.goToCreateProject} />
    //         <br/>
    //         <Button label="Upload partial deliverable" onClick={this.goToUploadPartialDeliverables} />
    //         <br/>
    //         <Button label="Vote" onClick={this.goToVote} />
    //         <br/>
    //         <br/>
    //         <br/>
    //         <br/>
    //         <Button label="Log out" onClick={this.goToLogin} />
    //         </div>
    //     </div>
    //     );
    //   }
    //   else
    //   {
    //     return(
    //     <div>
    //       <h1>Student Home</h1>
    //       <div id='dashboard'>
    //         <Button label="Create team" onClick={this.goToCreateTeam} />
    //         <br/>
    //         <Button label="Create project" onClick={this.goToCreateProject} />
    //         <br/>
    //         <Button label="Upload partial deliverable" onClick={this.goToUploadPartialDeliverables} />
    //         <br/>
    //         <Button label="Vote" onClick={this.goToVote} />
    //         <br/>
    //         <br/>
    //         <br/>
    //         <br/>
    //         <Button label="Log out" onClick={this.goToLogin} />
    //         </div>
    //     </div>
    //     );
    //   }
    //     ///
    }
}

export default withRouter(homeStudent);