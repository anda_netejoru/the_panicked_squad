import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './LoginUser.css';
const HostIp=process.env.REACT_APP_IP;
class LoginUser extends React.Component{
     constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:'',
            loggedUser:{}
        };
      //daca scrii ceva.. noi am facut in loginUser care citeste din baza de date
    
    //   this.handleChange=(evt)=>{
    //       this.setState({
    //           [evt.target.name]:evt.target.value
    //       })
    //       //console.log([evt.target.name])
    //   }  
      
    }
     handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value,
           
        });
    }
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }
    
    
    loginUser = () => {
        const user = this.state;
        axios.get(`http://${HostIp}:8080/api/users/email/${user.email}/password/${user.password}`, user).then(res => {
            const userFromDB=res.data;
           
           //schimb valoarea lui loggeduser
            this.setState({
                loggedUser:userFromDB
            });
            
           
           //trimit loggedUser catre App
            this.props.sendUser(this.state.loggedUser);
            
           if(userFromDB.role==='TEACHER'||userFromDB.role==='teacher'){
                window.location=`http://${HostIp}:3000/#/home-teacher`;
           }
           if(userFromDB.role==='STUDENT'||userFromDB.role==='student'){
                window.location=`http://${HostIp}:3000/#/home-student`;
           }
          //console.log(this);
        }).catch(err => {
            console.log(err);
            alert("Wrong credentials!")
        })
       
    }
   
    render(){
        return(
        <div>
        Login
        <br/>
            <div className='add-user-layout'>
            <br/>

            <span className="p-float-label">
           
                <InputText id="idEmail" value={this.state.email} onChange={this.handleChangeEmail} />
                <label htmlFor="in">Email</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            
                <span className="p-float-label">
                <InputText id="idPassword" value={this.state.password} onChange={this.handleChangePassword} />
                <label htmlFor="idPassword">Password</label>
                <br/>
                <br/>
                <br/>
                <br/>
             </span>

                <br/>
                <Button id='btnLoginUser' label="Login" onClick={this.loginUser} />
                <br/>
                <br/>
            </div>
        </div>    
        );
    }
}

export default withRouter(LoginUser);