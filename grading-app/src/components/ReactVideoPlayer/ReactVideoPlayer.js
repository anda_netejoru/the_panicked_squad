import React, { Component } from 'react'
import ReactPlayer from 'react-player'
import './ReactVideoPlayer.css';
class ReactVideoPlayer extends Component {
  render () {
    return <ReactPlayer url={this.props.link} playing />
  }
}
export default ReactVideoPlayer;