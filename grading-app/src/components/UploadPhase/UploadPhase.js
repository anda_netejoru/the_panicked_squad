import React from 'react';
import axios from 'axios';
 import {InputText} from 'primereact/inputtext';
//import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { withRouter } from 'react-router-dom'
import './UploadPhase.css';


const HostIp=process.env.REACT_APP_IP;

class UploadPhase extends React.Component{
    
     constructor(props) {
        super(props);
        this.state = {
            role: 'teacher',
             grades: [],
             clicked:false,
             phaseNumber:1,
             link: '',
             projectId: null,
             team:{}
             
        };
       
    }
    
    componentDidMount= ()=>{
     // console.log(this.props.receiveTeam.teamName);
        axios.get(`http://${HostIp}:8080/api/teams/teamName/${this.props.receiveTeam.teamName}`).then(res=>{
            this.setState({team:res.data[0]});
            this.setState({projectId:res.data[0].FKprojectId});
            //console.log("sequelize res",res.data);
            console.log(this.state.team);
        }).catch(err => {
            console.log(err);
            alert("No user belongs to this team !")
        })
        console.log(this.state.team);
    }
    
     handleChangePhaseNo = (event) => {
         
        this.setState({
            phaseNumber: event.target.value,
           
        });
     }
        
    handleChangeLink = (event) => {
         
        this.setState({
            //link: event.target.value.replace(/\//g, "")
            link: event.target.value
        });
       console.log(this.state.link);
    }
    
    handleUpdatePhaseLink = () => {
    console.log("projectId",this.state.projectId,"PhaseNumber",this.state.phaseNumber,"link",this.state)    
    axios.put(`http://${HostIp}:8080/api/phases/project/${this.state.projectId}/id/${this.state.phaseNumber}/link`,this.state)
        .then(res => {
        console.log(res);
        console.log(res.data);
        alert("Link uploaded!");
     }).catch(err => {
            console.log(err);
           alert("Error uploading link")
        })
             
          
        
    }
    
  
 goToHome=()=>{
  window.location=`http://${HostIp}:3000/#/home-student`;
}
    
    render(){
         return(
        <div>
        {/*this.state.team.id*/} {/*this.state.team.teamName*/} {/*this.state.team.FKuserId*/}
            <br/>
            <div id="home-teacher-title">
             {/*this.props.receiveTeam.teamName*/}
            <h1>Upload Phase</h1>
            </div>
            
            <br/>
            <br/>
            <Button label="Home" onClick={this.goToHome} />
            <br/>
            <br/>
            <br/>
            <br/>
            <div className='add-phase-layout'>
            <br/>
            <span className="p-float-label">
                <InputText id="idPhaseNo" value={this.state.phaseNumber} onChange={this.handleChangePhaseNo} />
                <label htmlFor="in">Phase number</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
            <div id='space-div'></div>
            <span className="p-float-label">
                <InputText id="idLink" value={this.state.link} onChange={this.handleChangeLink} />
                <label htmlFor="in">Link</label>
                <br/>
                <br/>
                <br/>
                <br/>
            </span>
           
            
             <Button  id='btnSubmit' label="Submit" onClick={this.handleUpdatePhaseLink} />

         
            </div>
             
            <br/>
            <br/>
            
        </div>
        ); 
        
    }
}

export default withRouter(UploadPhase);