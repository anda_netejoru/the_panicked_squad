import React, { Component } from 'react';
import {HashRouter as Router, Switch, Route} from 'react-router-dom'
import AddUser from './components/RegisterUser/AddUser';
import LoginUser from './components/LoginUser/LoginUser';
// import UserList from './components/UserList/UserList';
import axios from 'axios';
import './App.css';
import {Button} from 'primereact/button';
import CreateTeam from './components/CreateTeam/CreateTeam';
import HomeTeacher from './components/HomeTeacher/HomeTeacher';
import HomeStudent from './components/HomeStudent/HomeStudent';
import CreateProject from './components/CreateProject/CreateProject';
import UploadPhase from './components/UploadPhase/UploadPhase';
import Vote from './components/Vote/Vote';
const HostIp=process.env.REACT_APP_IP;

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      users:[],
      loggedUser:{},
      team:{}
    };
    
    this.changeLoggedUser=(updatedLoggedUser)=>{
      this.setState({
        loggedUser:updatedLoggedUser
      })
    }
    
    this.receiveTeam=(recteam)=>{
        this.setState({
            team:recteam
        })
    }
 
  }

  onUserAdded = (user) => {
    const users = this.state.users;
    users.push(users);
    this.setState({
      users: users
    });
  }
  

  //in componentDidMount - facem toate geturile pentru ca altfel se creeaza o bucla infinita
  componentDidMount = () => {
  
    axios.get(`http://${HostIp}:8080/api/users`).then(users => {
      this.setState({
        users: users.data
      })
    })
  }
  
goToLogin=()=>{
  window.location=`http://${HostIp}:3000/#/login`;
}
goToRegister=()=>{
  window.location=`http://${HostIp}:3000/#/register`;
} 
goToRoot=()=>{
  window.location=`http://${HostIp}:3000/#/`;
}
  render() {
    return (
      <div className="App">
      <Router>
      <Switch>
      <Route path="/" exact={true} >
          <div className="root-page">
         I dunno how you got there
          <br/>
          <br/>
          <Button label=" Click here to login" onClick={this.goToLogin}/>
          <br/>
          <br/>
           <Button label=" Click here to register" onClick={this.goToRegister}/>
           <br/>
           <br/>
          </div>
         </Route>
        <Route path="/home-student/create-team" exact={false} >
         <CreateTeam loggedUser={this.state.loggedUser} sendTeam={this.receiveTeam}/>
        </Route>
        
        <Route path="/home-student/create-project" exact={false} >
         <CreateProject receiveTeam={this.state.team}/>
        </Route>
        
        <Route path="/home-student/upload-phase" exact={false} >
         <UploadPhase receiveTeam={this.state.team}/>
        </Route>
        
        <Route path="/home-student/vote" exact={false} >
         <Vote loggedUser={this.state.loggedUser}/>
        </Route>
        
        <Route path="/home-student" exact={false} >
         <HomeStudent receiveTeam={this.state.team}/>
         </Route>
        
        <Route path="/register" exact={true} >
            <div className="add-user">
            <AddUser onUserAdded={this.onUserAdded}/>
            </div>
        </Route>
        
         <Route path="/login" exact={true} >
         {/* this.changeLoggedUser declari in constructorul app*/}
          <LoginUser sendUser={this.changeLoggedUser} loginUser={this.loginUser}/>
         </Route>
         
         <Route path="/home-teacher" exact={false} >
         <HomeTeacher/>
         </Route>
         
        
         
         <Route path="/error" exact={true} >
          404 not found
          <br/>
           <Button label="Go back" onClick={this.goToRoot}/>
         </Route>
         
      </Switch>
      </Router>
      </div>
    );
  }
}

export default App;
