

# API CALLS

>Postman requests will follow the next template : _IP_:_PORT_ _/PATH_
>Because cloud9 changes the ip after each time you login in , for convenience I will  put like this: 
>_:8080_ _/PATH_  
>and you will complete with  what IP you will have
>For testing purposes we also added some instances in order to test the endpoints
# User


### POST
- ####  _Create a user_
- ```:8080/api/user```
- #### Body

```
{
	"id":8,
	"role":"student",
	"email":"ceva8@gmail.com",
	"password":"bkaasa"
}
```
- ![picture](./ss/1.png)
---
### GET

- ####  _Get all users_
- ```:8080/api/users```
- ![picture](./ss/2.png)
---
- ####  _Get a user by id_
- ```:8080/api/users/id/:id```
- #### _Exemple_
- ```:8080/api/users/id/2```
- ![picture](./ss/3.png)

----------
- ####  _Get a user by email and password_
- ```:8080/api/users/email/:email/password/:password```
- #### _Exemple_
- ```:8080/api/users/email/ceva1@gmail.com/password/etc1```
- ![picture](./ss/4.png)
 
----------
- ####  _Get a user by email_
- ```::8080/api//users/email/:email/```
- #### _Exemple_
- ```:8080/api/users/email/ceva1@gmail.com/```

----------
- ####  _Get a user by the role_
- ```:8080/api/users/role/:role```
- #### _Exemple_
- ```:8080/api/users/role/student```
- ![picture](./ss/5.png)


----------

# Project


### POST

- #### _Creates a project along with the specified number of phases_

- ```:8080/api/project/teamName/racheta```
- #### Body:
```
{
	"id":89,
	"theme":"Ceva",
	"finalGrade":9.6,
	"finalDeadline":"2019-08-09",
	"noPhases":5
}
```
-  ![picture](./ss/6.png)
---------------------------------------------------------------
### GET
- #### _Gets all projects_
- ```:8080/api/projects```
- ![picture](./ss/7.png)

---------------------------------------------------------------
- #### _Gets a project by id and by final deadline_
- ```:8080/api/project/id/:id/finalDeadline/:finalDeadline```
- #### _Exemple_
- ```:8080/api/project/id/1/finalDeadline/2019-08-09T04:05:02.000Z```
- ![picture](./ss/8.png)

-----------------------------------------------------------------
- #### _Gets  projects by final deadline_
- ```:8080/api/projects/finalDeadline/:finalDeadline```
- #### _Exemple_
- ```:8080/api/projects/finalDeadline/2019-08-09T04:05:02.000Z```

- #### _Gets a jury member by user id_
- ```:8080/api/jury-members/user-id/:userId```
- #### _Exemple_
- ```:8080/api/jury-members/user-id/1```

---------------------------------------------------------------
- #### _Gets a project by id_
- ```:8080/api/projects/id/:id```
- #### _Exemple_
- ```:8080/api/projects/id/2```
- ![picture](./ss/9.png)

---------------------------------------------------------------
- #### _Gets the grades of the project_
- ```:8080/api/projects/grades```
- ![picture](./ss/10.png)

---------------------------------------------------------------
- #### _Gets the sum of  grades of a phase_
- ```:8080/api/project/id/:projectId/sum```
- #### _Exemple_
- ```:8080/api/project/id/2/sum```
- ![picture](./ss/11.png)

---------------------------------------------------------------
- #### _Gets the number of jury members of a project_
- ```:8080/api/jury-members/number/project/id/:projectId```
- #### _Exemple_
- ```:8080/api/jury-members/number/project/id/2```
- ![picture](./ss/12.png)

---------------------------------------------------------------
- #### _Gets the maximum grade of a project given by the jury_
- ```:8080/api/jury-members/project/max/id/:projectId```
- ####  _Exemple_
- ```:8080/api/jury-members/project/max/id/2```
- ![picture](./ss/13.png)
__________
- #### _Gets the minimum grade of a project given by the jury_
- ```:8080/api/jury-members/project/min/id/:projectId```
- #### _Exemple_
- ```:8080/api/jury-members/project/min/id/2```
- ![picture](./ss/14.png)

---------------------------------------------------------------
### PUT
- #### _Updates the final grade  with a grade of a project selected by id_
- ```:8080/api/project/id/:id/grade/:finalGrade```
- #### _Exemple_
- ```:8080/api/project/id/2/grade/3```
- ![picture](./ss/15.png)

---------------------------------------------------------------
- #### _Updates the final grade  with the calculated grade of a project selected by id_
- ```:8080/api/project/id/:projectId/finalgrade/```
- #### _Exemple_
- ```:8080/api/project/id/2/finalgrade/```
- ![picture](./ss/16.png)
---------------------------------------------------------------

# Phase
### POST
- #### _Creates a phase_
- ```:8080/api/phase```
- #### Body:
```
{
	"id":200,
	"deadline":"2019-08-09",
	"link":"URL://link.dnxxxs",
	"FKprojectId":1
}
```
- ![picture](./ss/17.png)

---------------------------------------------------------------
### GET
- #### _Gets all the phases_
- ```:8080/api/phases/```
- ![picture](./ss/18.png)

---------------------------------------------------------------
- #### _Gets all the phases of a project selected by project id_
- ```:8080/api/phases/id/:id```
- #### _Exemple_
- ```:8080/api/phases/id/2```
- ![picture](./ss/19.png)

---------------------------------------------------------------
- #### _Gets all the phases that have a specified deadline_
- ```:8080/api/phases/deadline/:deadline```
- #### _Exemple_
- ```:8080/api/phases/deadline/2019-08-09```
- ![picture](./ss/20.png)

---------------------------------------------------------------



### PUT

- #### _Updates the link of a specific phase which belongs to a specific project_
- ```:8080/api/phases/project/:FKprojectId/id/:id/link```
- #### Body:
```
{
	"link":"URL://linkUpdated.dnxxxs",
}
```
- #### _Exemple_
- ```:8080/api/phases/project/1/id/1/link```
- ![picture](./ss/21.png)

---------------------------------------------------------------
- #### _Updates the deadline of a specific phase which belongs to a specific project_
- ```:8080/api/phases/project/:FKprojectId/id/:id/deadline/:deadline```
- #### _Exemple_
- ```:8080/api/phases/project/1/id/1/deadline/2000-08-09```
- ![picture](./ss/22.png)

---------------------------------------------------------------

### DELETE
- #### _Deletes a phase of a certain project by a deadline_
- ```:8080/api/phases/delete/project/2/deadline/2019-08-09T11:07:02.000Z```
- ![picture](./ss/23.png)


# Jury


### POST

- #### _Creates a jury member_
- ```:8080/api/jury-member```
- #### Body
```
{
    "id": 5,
    "grade": 8,
    "FKuserId": 1,
    "FKprojectId": 2
}
```
- ![picture](./ss/24.png)
----
- #### _Inserts a jury member for a project_
- ```:8080/api/jury-member/selected/project-id/:FKprojectId```
- #### Body
```
{
  
}
```



---------------------------------------------------------------
### GET

- #### _Gets all the jury members_
- ```:8080/api/jury-members```
- ![picture](./ss/25.png)

---------------------------------------------------------------
- #### _Gets all the jury members that jury a project (selected by porject id)_
- ```:8080/api/jury-members/id/:id```
- #### _Exemple_
- ```:8080/api/jury-members/id/2```
- ![picture](./ss/26.png)

---------------------------------------------------------------

### PUT

- #### _Updates the grade of a jury on a certain project(jury ID and project ID)_
- ```:8080/api/jury-member/id/:id/project/:FKprojectId/grade/:grade```
- #### _Exemple_
- ```:8080/api/jury-member/id/1/project/2/grade/10```
- ![picture](./ss/27.png)



# Team 


### POST
- #### _Creates a team_
- ```:8080/api/team```
- #### Body
```
{
	"id":7,
	"teamName":"racheta",
	"FKuserId":1,
	"FKprojectId":1
}
```
- ![picture](./ss/28.png)
---------------------------------------------------------------

### GET

- #### _Gets all the teams_
- ```:8080/api/teams```
- ![picture](./ss/29.png)
---------------------------------------------------------------
- #### _Gets all the team members by team name_
- ```:8080/api/teams/teamName/:teamName```
- #### _Exemple_
- ```:8080/api/teams/teamName/racheta```
- ![picture](./ss/30.png)

---------------------------------------------------------------
- #### _Gets a team by project id_
- ```:8080/api/team/project-id/:FKprojectId```
- #### _Exemple_
- ```:8080/api/team/project-id/1```

---------------------------------------------------------------
- #### _Get a Team object (a user basically) that is eligible for jury_
- ```:8080/api/teams/project/:FKprojectId```
- #### _Exemple_
- ```:8080/api/teams/project/2```
- ![picture](./ss/31.png)

---------------------------------------------------------------
### PUT

- #### _Updates the  project id of all users that are in the same team (returns the number of affected rows)_
- ```:8080/api/teams/teamName/:teamName/project/:FKprojectId```
- #### _Exemple_
- ```:8080/api/teams/teamName/racheta/project/20```
- ![picture](./ss/32.png)

- - Before:
- -  ![picture](./ss/30_1.png)
-  - After:
- -  ![picture](./ss/32_1.png)

# Server STUFF:

### GET
- #### _Gets the current time of the server and saves it_
- ```:8080/api/time```
- ![picture](./ss/33.png)

---------------------------------------------------------------
- #### _Gets all the server time logs_
- ```:8080/api/times```
- ![picture](./ss/34.png)

### DELETE
- #### _Deletes all the server time logs_
- ```:8080/api/times/delete```
- ![picture](./ss/35.png)
- - Before:
- -![picture](./ss/35_1.png)
- - After:
- - ![picture](./ss/35_2.png)
